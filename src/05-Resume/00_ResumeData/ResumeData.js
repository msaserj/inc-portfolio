const resume = {
  education: {
    left: {
      years: '2000-2010',
      title: 'State Education',
      items: [
        {
          title: 'Kostroma School of Secondary Education',
          subtitle: 'School of Secondary Education',
          years: '2000-2003',
          description: 'School of Secondary Education'
        },
        {
          title: 'Kostroma Energy College named after F.V. Chizhov.',
          subtitle: 'Secondary vocational education.',
          years: '2003-2006',
          description:
            ' Installation, adjustment and operation of electrical equipment of industrial and civil buildings.'
        },
        {
          title: 'Kostroma State Agricultural Academy.',
          subtitle: 'Higher education school. Grade: master`s degree',
          years: '2006-2010',
          description: 'Engineer specializing in electrification and automation of agriculture.'
        }
      ]
    },
    right: {
      years: '2000-2022',
      title: 'Self Education',
      items: [
        {
          title: 'Circle of radio electronics',
          subtitle: 'Extracurricular educations',
          years: '2000-2002',
          description: 'Radio electronics course.'
        },
        {
          title: 'Udemy: The Complete 2021-2022 Web Development Bootcamp',
          subtitle: 'Web Development Bootcamp',
          years: '2021-2022',
          description: 'The main concepts of back-end and front-end web development are relevant for 2021.'
        },
        {
          title: 'IT-Incubator',
          subtitle: 'IT-Incubator Minsk, Belarus.',
          years: '2022-Present',
          description:
            'IT incubator is not IT courses, but an incubator. Not only study but also practical team development of your own project.'
        }
      ]
    }
  },
  experience: {
    left: {
      years: '2011-2022',
      title: 'commercial experience',
      items: [
        {
          title: '«Kostromskoi zavod avtokomponentov» JSC',
          subtitle: 'Department of Information Security. Engineer',
          years: '2011-2015',
          description:
            'Leading specialist in security systems.' +
            '• Installation of security systems: OPS Orion Pro, ACS Parsec, video surveillance Domination.' +
            '• Setting up network equipment and workstations running Windows OS, support for network users.' +
            '• Administration of enterprise servers Windows Server 2003-2008.' +
            '• Designed and implemented projects of fire and security alarms, video surveillance and access ' +
            'control systems. Documentation for the maintenance of security systems has been developed.' +
            '• Negotiations with suppliers, verification of contractors, conclusion of contracts.'
        },
        {
          title: '«Kostromskoi zavod avtokomponentov» JSC',
          subtitle: 'Lead Technical Support Specialist, System Administrator.',
          years: '2015-2020',
          description:
            'Note. The information security department was reduced and merged with the administration and technical support department.' +
            ' Administration of network infrastructure with a fleet of 500+ workstations, Windows/Linux in the ProxMox virtualization system.' +
            ' Our team has implemented several projects: transferred terminal servers and user workstations ' +
            'to Windows OS, implemented a project to divide the enterprise network into separate VLAN virtual' +
            " networks. The company's wireless network was upgraded using unify equipment. Also maintenance" +
            ' of enterprise security systems.'
        }
      ]
    },
    right: {
      years: '2011-2022',
      title: 'commercial experience',
      items: [
        {
          title: '«Electromontaj» JC',
          subtitle: 'System Administrator.',
          years: '2020-2022',
          description:
            'System Administrator Administration of network infrastructure.Translation of users to a new domain controller. ' +
            'Implementation of the Zabbix monitoring system. Implemented the Unifi wireless network. ' +
            'The network infrastructure is put in order. Support for users in the office and in remote areas in the cities of Russia.'
        },
        {
          title: 'Frontend developer',
          subtitle: 'React Front-end Developer | Part-time',
          years: '2020-2022',
          description:
            'Team development of a training application for a ready-made API. Managed the development team.' +
            'Technologies and tools used: React, Redux, TypeScript, ReduxThunk, RestApi, Axios, Formik, MaterialUI, SCSS etc.' +
            'Development of a social network application' +
            'Development of the interface of the to-do list application using UnitTests and StoryBook'
        }
      ]
    }
  },
  interview: {
    left: {
      years: '2017-2019',
      title: 'running',
      items: [
        {
          title: 'ParkRun',
          subtitle: 'ParkRun Berendeyevka Kostroma',
          years: '2017-2019',
          description:
            'I am into long distance running. And I took part in the organization of weekly park races' +
            ' at a distance of 5 km for everyone and the creation of the Kostroma community of runners.'
        }
      ]
    },
    right: {
      years: '2017-2019',
      title: 'running',
      items: [
        {
          title: 'TrailRun Event',
          subtitle: 'Susanin Trail Forest Race',
          years: '2019',
          description:
            'Our Kostroma team of runners SusaninRun organized a race for everyone runners.' +
            ' It was a very interesting and useful experience in organizing such events.' +
            ' We organized not just a boring race, but a running festival.'
        }
      ]
    }
  },
  skills: {
    left: {
      years: 'features',
      title: 'Development Skills',
      items: [
        {
          title: 'HTML',
          percent: 90
        },
        {
          title: 'CSS/SCSS',
          percent: 85
        },
        {
          title: 'JS/TS',
          percent: 75
        },
        {
          title: 'React',
          percent: 78
        },
        {
          title: 'Redux',
          percent: 72
        },
        {
          title: 'MaterialUI',
          percent: 80
        },
        {
          title: 'Formik',
          percent: 80
        },
        {
          title: 'ExpressJS',
          percent: 50
        }
      ]
    },
    right: {
      years: 'features',
      title: 'Other Skills',
      items: [
        {
          title: 'Webstorm',
          percent: 75
        },
        {
          title: 'VSCode',
          percent: 65
        },
        {
          title: 'Git',
          percent: 80
        },
        {
          title: 'Storybook',
          percent: 75
        },
        {
          title: 'LinuxMint',
          percent: 75
        },
        {
          title: 'Debian',
          percent: 70
        },
        {
          title: 'NodeJS',
          percent: 60
        },
        {
          title: 'Mongodb',
          percent: 50
        }
      ]
    }
  }
};
export default resume;
